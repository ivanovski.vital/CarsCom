﻿using Framework.Utils;

namespace CarsCom.Utils
{
    public enum EnumTopMenu
    {
        [StringEnum.StringValue("//a[contains(@data-linkname, 'buy')]")] Buy,
        [StringEnum.StringValue("//a[contains(@data-linkname, 'sell')]")] SellTrade
    }
}
