﻿using Framework.Utils;

namespace CarsCom.Utils
{
    public enum EnumCarMenu
    {
        [StringEnum.StringValue("//li[contains(@cars-tracking-omniture-custom, 'Search')]//div[contains(@ng-click, 'tab')]")] SearchCarsForSale,
        [StringEnum.StringValue("//li[contains(@cars-tracking-omniture-custom, 'Research')]//div[contains(@ng-click, 'tab')]")] ReadSpecsReviews
    }
}
