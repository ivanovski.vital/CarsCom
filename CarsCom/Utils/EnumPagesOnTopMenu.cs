﻿using Framework.Utils;

namespace CarsCom.Utils
{
    public enum EnumPagesOnTopMenu
    {
        [StringEnum.StringValue("//a[contains(@cui-omniture-standard, 'to-sell')]")] HowToSellYourCar,
        [StringEnum.StringValue("//a[contains(@data-linkname, 'used-car')]")] UsedCarForSale
    }
}
