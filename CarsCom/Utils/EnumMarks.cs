﻿using Framework.Utils;

namespace CarsCom.Utils
{
    public enum EnumMarks
    {
        [StringEnum.StringValue("//option[@label = 'Kia']")] KIA,
        [StringEnum.StringValue("//option[@label = 'Ford']")] Ford,
        [StringEnum.StringValue("//option[@label = 'Volkswagen']")] VW,
        Unknown
    }
}
