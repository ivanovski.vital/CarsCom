﻿using System;

namespace CarsCom.Utils
{
    public static class Constants
    {
        public const String ZIP = "55416";
        public const String Mileage = "20000";
        public const String ClickablePartOfFirstCarAd = "//a[@data-position= '1']";
        public const String DetectorOfDropSaleCar = "Phot";
    }
}
