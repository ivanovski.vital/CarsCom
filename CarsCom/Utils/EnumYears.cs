﻿using Framework.Utils;

namespace CarsCom.Utils
{
    public enum EnumYears
    {
        [StringEnum.StringValue("//option[@label = '2018']")] Y2018,
        [StringEnum.StringValue("//option[@label = '2014']")] Y2014
    }
}
