﻿using Framework.Utils;

namespace CarsCom.Utils
{
    public enum EnumModels
    {
        [StringEnum.StringValue("//option[@label = 'Rio']")] Rio,
        [StringEnum.StringValue("//option[@label = 'Edge']")] Edge,
        [StringEnum.StringValue("//option[@label = 'Touareg']")] Touareg,
        Unknown
    }
}
