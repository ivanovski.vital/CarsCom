﻿using Framework.Utils;

namespace CarsCom.Utils
{
    public enum EnumPrices
    {
        [StringEnum.StringValue("//option[@label = '10000']")] U10000,
        [StringEnum.StringValue("//option[@label = '$5,000']")] U5000,
        [StringEnum.StringValue("//option[@label = '$7,000']")] U7000
    }
}
