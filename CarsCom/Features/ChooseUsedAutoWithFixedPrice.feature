﻿Feature: ChooseUsedAutoWithFixedPrice

Scenario: Choose used auto with fixed price
	Given The home page was opened
	When I click to Buy on topmenu
	Then Buy page is open

	When I click to Used car for sale
	Then The page Used car for sale is open

	When I check price until 7000$
		And I check Owner Seller checkbox
			And I check ZIP
				And I click Search Listing
	Then The page with used cars is open

	When I choose the first car on the cars for sale page as 'Reference_Car'
	Then The page with selected car is open
		And Reference data from 'Reference_Car' with data on the Specific car page are equal
 