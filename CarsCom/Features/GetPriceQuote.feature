﻿Feature: GetPriceQuotefeature


Scenario: Go to Price Quote page with a specific car
	Given The home page was opened
	When I click item Read Spec
	Then Item successfully open

	When I choice Ford as mark of car
		And I choice Edge as model of car
			And I choice 2018 as year
				And I memorize car data as 'Reference_car' on the home page
	Then correct data stay in the fields of Read Specs & Reviews

	When I click Search to overview
	Then overview page is open
		And Reference data from 'Reference_car' with data on the Overview page are equal

	When I click Get a Price Quote
	Then Price Quote page is open
		And Data on the page coincide with data from 'Reference_car'
