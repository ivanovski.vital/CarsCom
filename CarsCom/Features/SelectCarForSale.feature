﻿Feature: SelectCarForSale


Scenario: Selecting car for sale
	Given The home page was opened
		And Search cars for sale tab was opened
	When I enter a reference ZIP code
		And I choice KIA as mark of car
			And I choice Rio as model of car
	Then correct data stay in the fields of Search car for sale
	
	When I click Search cars for sale
	Then The page cars for sale is open
	
	When I choose the first car on the cars for sale page as 'Reference_Car'
	Then The page with selected car is open
		And Reference data from 'Reference_Car' with data on the Specific car page are equal
	