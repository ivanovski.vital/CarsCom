﻿Feature: GetCarValue

Scenario: Get car value
	Given The home page was opened
	When I click to Sell & Trade on topmenu
	Then Sell & Trade page is open

	When I click to How sell your car
	Then The page How sell your car is open

	When I click Find Your Car's Value
	Then The page Find Value is open

	When I input Volkswager as mark
		And I input Touareg as model
			And I input 2014 as year
				And I input 4drX as style
					And I input 20000 as mileage
						And I input ZIP
							And I memorize car data as 'Reference_car' on the Find Value page
	Then Correct data stay in the fields of Find Value page

	When I click Get Estimate
	Then Estimator page is open
		And All data coincide with 'Reference_car'

