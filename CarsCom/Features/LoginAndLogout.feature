﻿Feature: LoginAndLogout


Scenario: User logging in the system then quit
	Given The home page was opened
	When I click to icon with avatar
	Then Popup with user settings appear

	When I click login in the system
	Then Login page is open

	When I input user data on the fields
		And I click Log in
	Then home page is open
		And User successfully logging in the system

	When I click to icon with avatar
	Then Popup with user settings appear

	When I click Log out
	Then home page is open
		And User successfully logouting from the system
