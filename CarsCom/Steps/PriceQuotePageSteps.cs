﻿using CarsCom.Pages;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class PriceQuotePageSteps
    {
        [Then(@"Price Quote page is open")]
        public void PriceQuotePageIsOpen()
        {
            PriceQuotePage priceQuote = new PriceQuotePage();
        }

        [Then(@"Data on the page coincide with data from '(.*)'")]
        public void DataOnThePageCoincideWithDataFrom(string referenceCar)
        {
            PriceQuotePage priceQuote = new PriceQuotePage();
            priceQuote.CheckDataAboutCar(ScenarioContext.Current[referenceCar]);
        }
    }
}
