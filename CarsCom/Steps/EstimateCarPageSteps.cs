﻿using CarsCom.Pages;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class EstimateCarPageSteps
    {
        [Then(@"Estimator page is open")]
        public void EstimatorPageIsOpen()
        {
            EstimateCarPage estimateCar = new EstimateCarPage();
        }

        [Then(@"All data coincide with '(.*)'")]
        public void AllDataCoincideWith(string referenceCar)
        {
            EstimateCarPage estimateCar = new EstimateCarPage();
            estimateCar.CheckReferenceValues(ScenarioContext.Current[referenceCar]);
        }
    }
}
