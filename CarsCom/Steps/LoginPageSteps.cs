﻿using CarsCom.Pages;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class LoginPageSteps
    {
        [Then(@"Login page is open")]
        public void LoginPageIsOpen()
        {
            LoginPage login = new LoginPage();
        }

        [When(@"I input user data on the fields")]
        public void InputUserDataOnTheFields()
        {
            LoginPage login = new LoginPage();
            login.FillUserData();
        }

        [When(@"I click Log in")]
        public void WhenIClickLogIn()
        {
            LoginPage login = new LoginPage();
            login.ConfirmLogin();
        }
    }
}
