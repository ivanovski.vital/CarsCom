﻿using CarsCom.Pages;
using CarsCom.Utils;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class UsedCarForSalePageSteps
    {
        [Then(@"The page Used car for sale is open")]
        public void PageUsedCarForSaleIsOpen()
        {
            UsedCarForSalePage usedCarForSale = new UsedCarForSalePage();
        }

        [When(@"I check price until (.*)\$")]
        public void CheckPriceUntil(int p0)
        {
            UsedCarForSalePage usedCarForSale = new UsedCarForSalePage();
            usedCarForSale.InputPrice(EnumPrices.U7000);
        }

        [When(@"I check Owner Seller checkbox")]
        public void CheckOwnerSellerCheckbox()
        {
            UsedCarForSalePage usedCarForSale = new UsedCarForSalePage();
            usedCarForSale.CheckOwnerSeller();
        }

        [When(@"I click Search Listing")]
        public void ClickSearchListing()
        {
            UsedCarForSalePage usedCarForSale = new UsedCarForSalePage();
            CarsForSalePage carsForSale = usedCarForSale.SearchListing();
        }

        [When(@"I check ZIP")]
        public void WhenICheckZIP()
        {
            UsedCarForSalePage usedCarForSale = new UsedCarForSalePage();
            usedCarForSale.CheckZIP();
        }
    }
}
