﻿using CarsCom.Pages;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class CarsForSalePageSteps
    {
        [Then(@"The page cars for sale is open")]
        public void PageCarsForSaleIsOpen()
        {
            CarsForSalePage carsForSale = new CarsForSalePage();
        }

        [When(@"I choose the first car on the cars for sale page as '(.*)'")]
        public void ChooseTheFirstCarOnThePageAs(string referenceCar)
        {
            CarsForSalePage carsForSale = new CarsForSalePage();
            ScenarioContext.Current.Add(referenceCar, carsForSale.carsList.GetFirstCar());
            carsForSale.ChooseFirstCar();
        }

        [Then(@"The page with used cars is open")]
        public void PageWithUsedCarsIsOpen()
        {
            CarsForSalePage carsForSale = new CarsForSalePage();
        }
    }
}
