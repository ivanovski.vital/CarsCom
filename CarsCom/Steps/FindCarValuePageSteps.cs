﻿using CarsCom.Pages;
using CarsCom.Utils;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class FindCarValuePageSteps
    {
        [Then(@"The page Find Value is open")]
        public void PageFindValueIsOpen()
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
        }

        [When(@"I input Volkswager as mark")]
        public void InputVolkswagerAsMark()
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.InputSpecificMark(EnumMarks.VW);
        }

        [When(@"I input Touareg as model")]
        public void InputTouaregAsModel()
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.InputSpecificModel(EnumModels.Touareg);
        }

        [When(@"I input (.*) as year")]
        public void Input2014AsYear(int p0)
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.InputSpecificYear(EnumYears.Y2014);
        }

        [When(@"I input (.*)drX as style")]
        public void Input4DrXAsStyle(int p0)
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.InputSpecificStyle(EnumCarStyles.drX_Touareg);
        }

        [When(@"I input (.*) as mileage")]
        public void Input20000AsMileage(int p0)
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.InputSpecificMileage(Constants.Mileage);
        }

        [When(@"I input ZIP")]
        public void InputZIP()
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.InputZIP();
        }

        [When(@"I memorize car data as '(.*)' on the Find Value page")]
        public void MemorizeCarDataAsOnTheFindValuePage(string referenceCar)
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            ScenarioContext.Current.Add(referenceCar, findCarValue.GetCar());
        }

        [Then(@"Correct data stay in the fields of Find Value page")]
        public void CorrectDataStayInTheFieldsOfFindValuePage()
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.CheckInputedData();
        }

        [When(@"I click Get Estimate")]
        public void ClickGetEstimate()
        {
            FindCarValuePage findCarValue = new FindCarValuePage();
            findCarValue.EstimateCar();
        }
    }
}
