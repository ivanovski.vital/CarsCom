﻿using CarsCom.Pages;
using CarsCom.Utils;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class BuyPageSteps
    {
        [Then(@"Buy page is open")]
        public void BuyPageIsOpen()
        {
            BuyPage buy = new BuyPage();
            buy.CheckOpenedItemOnTopMenu(EnumTopMenu.Buy);
        }

        [When(@"I click to Used car for sale")]
        public void ClickToUsedCarForSale()
        {
            BuyPage buy = new BuyPage();
            buy.OpenPageFromOpenedItemOnTopMenu(EnumPagesOnTopMenu.UsedCarForSale);
        }
    }
}
