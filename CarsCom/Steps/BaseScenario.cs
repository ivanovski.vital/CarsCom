﻿using TechTalk.SpecFlow;
using OpenQA.Selenium;
using Framework.Browsers;
using Framework.Utils;
using log4net.Config;

namespace CarsCom.Steps
{
    [Binding]
    public class BaseScenario
    {
        [BeforeScenario()]
        public void Open()
        {
            XmlConfigurator.Configure();
            Log.log.Info("Open browser");
            BrowserManager manager = BrowserManager.GetInstance();
        }

        [AfterScenario()]
        public void Close()
        {
            BrowserManager manager = BrowserManager.GetBrowser();
            if (manager != null)
            {
                IWebDriver driver = manager.GetDriver();
                if (driver != null)
                {
                    Log.log.Info("Browser close");
                    driver.Close();
                    driver = null;
                    BrowserManager.setDriverNull();
                }
            }
        }
    }
}
