﻿using CarsCom.Pages;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class HowToSellYourCarPageSteps
    {
        [Then(@"The page How sell your car is open")]
        public void PageHowSellYourCarIsOpen()
        {
            HowToSellYourCarPage howToSellYourCar = new HowToSellYourCarPage();
        }

        [When(@"I click Find Your Car's Value")]
        public void ClickFindYourCarSValue()
        {
            HowToSellYourCarPage howToSellYourCar = new HowToSellYourCarPage();
            FindCarValuePage findCarValue = howToSellYourCar.OpenFindCarValue();
        }
    }
}
