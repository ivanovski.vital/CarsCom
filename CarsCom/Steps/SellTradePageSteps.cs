﻿using CarsCom.Pages;
using CarsCom.Utils;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class SellTradePageSteps
    {
        [Then(@"Sell & Trade page is open")]
        public void SellTradePageIsOpen()
        {
            SellTradePage sellTrade = new SellTradePage();
            sellTrade.CheckOpenedItemOnTopMenu(EnumTopMenu.SellTrade);
        }

        [When(@"I click to How sell your car")]
        public void ClickToHowSellYourCar()
        {
            SellTradePage sellTrade = new SellTradePage();
            sellTrade.OpenPageFromOpenedItemOnTopMenu(EnumPagesOnTopMenu.HowToSellYourCar);
        }
    }
}
