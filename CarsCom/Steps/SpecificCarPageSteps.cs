﻿using CarsCom.Pages;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class SpecificCarPageSteps
    {
        [Then(@"The page with selected car is open")]
        public void PageWithSelectedCarIsOpen()
        {
            SpecificCarPage specificCar = new SpecificCarPage();
        }

        [Then(@"Reference data from '(.*)' with data on the Specific car page are equal")]
        public void ReferenceDataFromWithDataOnTheSpecificCarPageAreEqual(string referenceCar)
        {
            SpecificCarPage specificCar = new SpecificCarPage();
            specificCar.CheckReferenceValues(ScenarioContext.Current[referenceCar]);
        }
    }
}
