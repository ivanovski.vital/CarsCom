﻿using CarsCom.Pages;
using CarsCom.Utils;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    class HomePageSteps : BaseScenario
    {
        [Given(@"The home page was opened")]
        public void HomePageWasOpened()
        {
            HomePage home = new HomePage();
        }

        [Given(@"Search cars for sale tab was opened")]
        public void GivenSearchCarsForSaleTabWasOpened()
        {
            HomePage home = new HomePage();
            home.SelectSpecificItemInMenuCar(EnumCarMenu.SearchCarsForSale);
        }

        [When(@"I click to icon with avatar")]
        public void ClickToIconWithAvatar()
        {
            HomePage home = new HomePage();
            home.OpenPopupMenuWithUserSettings();
        }

        [Then(@"Popup with user settings appear")]
        public void PopupMenuWithUserSettingsAppear()
        {
            HomePage home = new HomePage();
            home.CheckPopupMenuWithUserSettings();
        }

        [When(@"I click login in the system")]
        public void ClickLoginInTheSystem()
        {
            HomePage home = new HomePage();
            LoginPage login = home.LoginInTheSystem();
        }

        [Then(@"User successfully logging in the system")]
        public void UserSuccessfullyLoggingInTheSystem()
        {
            HomePage home = new HomePage();
            home.CheckLogging();
        }

        [Then(@"home page is open")]
        public void HomePageIsOpen()
        {
            HomePage home = new HomePage();
        }

        [When(@"I click Log out")]
        public void ClickLogOut()
        {
            HomePage home = new HomePage();
            home.LogoutFromTheSystem();
        }

        [Then(@"User successfully logouting from the system")]
        public void UserSuccessfullyLogoutingFromTheSystem()
        {
            HomePage home = new HomePage();
            home.CheckLogouting();
        }

        [When(@"I choice KIA as mark of car")]
        public void ChoiceKIAAsMarkOfCar()
        {
            HomePage home = new HomePage();
            home.SetMarkCar(EnumMarks.KIA);
        }

        [When(@"I choice Rio as model of car")]
        public void ChoiceRioAsModelOfCar()
        {
            HomePage home = new HomePage();
            home.SetModelCar(EnumModels.Rio);
        }

        [When(@"I enter a reference ZIP code")]
        public void EnterZIPCode()
        {
            HomePage home = new HomePage();
            home.EnterZIPCode();
        }

        [Then(@"correct data stay in the fields of Search car for sale")]
        public void CorrectDataStayInTheFieldsOfSearchCarForSale()
        {
            HomePage home = new HomePage();
            home.CheckDataInCarMenu();
        }

        [Then(@"correct data stay in the fields of Read Specs & Reviews")]
        public void CorrectDataStayInTheFieldsOfReadSpecsReviews()
        {
            HomePage home = new HomePage();
            home.CheckDataInCarMenu();
        }

        [When(@"I click Search cars for sale")]
        public void ClickSearchCarsForSale()
        {
            HomePage home = new HomePage();
            home.SearchCarsForSale();
        }

        [When(@"I click item Read Spec")]
        public void ClickItemReadSpec()
        {
            HomePage home = new HomePage();
            home.SelectSpecificItemInMenuCar(EnumCarMenu.ReadSpecsReviews);
        }

        [Then(@"Item successfully open")]
        public void ItemSuccessfullyOpen()
        {
            HomePage home = new HomePage();
            home.CheckSpecificItemInMenuCar(EnumCarMenu.ReadSpecsReviews);
        }

        [When(@"I choice 2018 as year")]
        public void ChoiceYear()
        {
            HomePage home = new HomePage();
            home.SetYearCar(EnumYears.Y2018);
        }

        [When(@"I memorize car data as '(.*)' on the home page")]
        public void MemorizeCarDataAs(string referenceCar)
        {
            HomePage home = new HomePage();
            ScenarioContext.Current.Add(referenceCar, home.GetCarData());
        }

        [When(@"I choice Ford as mark of car")]
        public void ChoiceFordAsMarkOfCar()
        {
            HomePage home = new HomePage();
            home.SetMarkCar(EnumMarks.Ford);
        }

        [When(@"I choice Edge as model of car")]
        public void ChoiceEdgeAsModelOfCar()
        {
            HomePage home = new HomePage();
            home.SetModelCar(EnumModels.Edge);
        }

        [When(@"I click Search to overview")]
        public void ClickSearchToOverview()
        {
            HomePage home = new HomePage();
            OverviewPage overview = home.SearchToOverview();
        }

        [When(@"I click to Sell & Trade on topmenu")]
        public void ClickToSellTradeOnTopmenu()
        {
            HomePage home = new HomePage();
            home.OpenItemOnTopMenu(EnumTopMenu.SellTrade);
        }

        [When(@"I click to Buy on topmenu")]
        public void ClickToBuyOnTopmenu()
        {
            HomePage home = new HomePage();
            home.OpenItemOnTopMenu(EnumTopMenu.Buy);
        }
    }
}
