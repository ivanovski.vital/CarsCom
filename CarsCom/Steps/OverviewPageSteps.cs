﻿using CarsCom.Pages;
using TechTalk.SpecFlow;

namespace CarsCom.Steps
{
    [Binding]
    class OverviewPageSteps
    {
        [Then(@"overview page is open")]
        public void OverviewPageIsOpen()
        {
            OverviewPage overview = new OverviewPage();
        }

        [Then(@"Reference data from '(.*)' with data on the Overview page are equal")]
        public void ReferenceDataFromWithDataOnTheOverviewPageAreEqual(string referenceCar)
        {
            OverviewPage overview = new OverviewPage();
            overview.CheckReferenceValues(ScenarioContext.Current[referenceCar]);
        }

        [When(@"I click Get a Price Quote")]
        public void ClickGetAPriceQuote()
        {
            OverviewPage overview = new OverviewPage();
            PriceQuotePage priceQuote = overview.ClickToGetAPriceQuote();
        }
    }
}
