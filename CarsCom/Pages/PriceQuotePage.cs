﻿using CarsCom.Elements;
using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class PriceQuotePage : BasePage
    {
        private String actualMark, actualModel, actualYear;
        private const String priceQuotePageMessage = "Price quote page didn't open";
        private const String dataAboutCarMessage = "Data about the car didn't open";

        private static readonly By priceQuotePageLocator = By.XPath("//h1[contains(@class, 'price-quote')]");
        private static readonly By dataAboutCarLocator = By.XPath("//div[contains(@class, 'test')]//b");

        public PriceQuotePage() : base(priceQuotePageLocator, priceQuotePageMessage) { }

        private Car currentCar;

        public void CheckDataAboutCar(Object car)
        {
            Log.log.Info("Check data about car on price quote page");
            actualYear = BaseElement.GetTextFromField(dataAboutCarLocator);
            actualYear = RegexParser.Parse(actualYear, StringEnum.GetStringValue(EnumRegExs.PureIntDigit))[0].ToString();

            actualMark = BaseElement.GetTextFromField(dataAboutCarLocator);
            actualMark = RegexParser.Parse(actualMark, StringEnum.GetStringValue(EnumRegExs.PureLetters))[0].ToString();

            actualModel = BaseElement.GetTextFromField(dataAboutCarLocator);
            actualModel = RegexParser.Parse(actualModel, StringEnum.GetStringValue(EnumRegExs.PureLetters))[1].ToString();

            currentCar = new Car(actualMark, actualModel, actualYear);
            if (!currentCar.Equals(car))
            {
                FWAssert.Fail("Data from specific car not equal");
            }
        }
    }
}
