﻿using CarsCom.Elements;
using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class EstimateCarPage : BasePage
    {
        private Car referenceCar;
        private String carData;
        private const String estimateCarPageMessage = "Estimate car page didn't open'";
        private static readonly By estimateCarPageLocator = By.XPath("//div[contains(@class, 'flex')]");
        private static readonly By markModelStyleLocator = By.XPath("//h2[contains(@class, 'gamma')]");

        public EstimateCarPage() : base(estimateCarPageLocator, estimateCarPageMessage) { }

        public void CheckReferenceValues(Object car)
        {
            Log.log.Info("Check reference values on Estimate car page");
            referenceCar = car as Car;
            carData = BaseElement.GetTextFromField(markModelStyleLocator);

            if (!referenceCar.ContainEqualsMMS(carData))
            {
                FWAssert.Fail("Data from specific car not equal");
            }
        }
    }
}
