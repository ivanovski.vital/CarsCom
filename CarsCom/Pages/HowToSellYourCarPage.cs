﻿using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class HowToSellYourCarPage : BasePage
    {
        private const String howtoSellMessage = "How to Sell Your Car page didn't open'";
        private const String btnFindCarValueMessage = "'Find car's Value' button didn't open'";

        private static readonly By howToSellLocator = By.XPath("//section[contains(@id, 'prepare')]");
        private static readonly By btnFindCarValueLocator = By.XPath("//a[contains(@href, 'book-value') and contains(@href, 'cars')]");

        public HowToSellYourCarPage() : base(howToSellLocator, howtoSellMessage){ }

        private readonly Button btnFindCarValue = new Button(btnFindCarValueLocator, btnFindCarValueMessage);

        public FindCarValuePage OpenFindCarValue()
        {
            Log.log.Info("Open Find Car Value");
            btnFindCarValue.Click();
            return new FindCarValuePage();
        }
    }
}
