﻿using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class LoginPage : BasePage
    {
        private const String loginPageMessage = "Login page didn't open";
        private const String txbUsernameMessage = "Textbox 'Username' didn't open";
        private const String txbPasswordMessage = "Textbox 'Password' didn't open";
        private const String btnLoginMessage = "Button 'Login' didn't open";

        private static readonly By loginPageLocator = By.XPath("//div[contains(@class, 'profiles__e')]");
        private static readonly By txtUsernameLocator = By.XPath("//input[contains(@type, 'tex')]");
        private static readonly By txtPasswordLocator = By.XPath("//input[contains(@type, 'pass')]");
        private static readonly By btnLoginLocator = By.XPath("//div[contains(@class, 'email')]//button[contains(@class, 'button')]");

        public LoginPage() : base(loginPageLocator, loginPageMessage) { }

        private Lazy<Textbox> txtUsername = new Lazy<Textbox>(()=> new Textbox(txtUsernameLocator, txbUsernameMessage));
        private Lazy<Textbox> txtPassword = new Lazy<Textbox>(() => new Textbox(txtPasswordLocator, txbPasswordMessage));
        private Lazy<Button> btnLogin = new Lazy<Button>(() => new Button(btnLoginLocator, btnLoginMessage)); 

        public LoginPage FillUserData()
        {
            Log.log.Info("Fill user data on Login Page");
            txtUsername.Value.SendKeys(Constants.login);
            txtPassword.Value.SendKeys(Constants.password);
            return new LoginPage();
        }

        public HomePage ConfirmLogin()
        {
            Log.log.Info("Confirm login");
            btnLogin.Value.Click();
            return new HomePage();
        }
    }
}
