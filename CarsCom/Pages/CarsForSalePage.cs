﻿using CarsCom.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class CarsForSalePage : BasePage
    {
        private const String carsForSalePageMessage = "Cars for sale page didn't open";
        private const String firstCarMessage = "First car on page didn't open";
        private static readonly By carsForSalePageLocator = By.XPath("//div[contains(@class, 'header-prim')]//div[contains(@class, 'save-sear')]");

        public CarsForSalePage() : base(carsForSalePageLocator, carsForSalePageMessage){ }

        public CarsList carsList = new CarsList();

        public void ChooseFirstCar()
        {
            Log.log.Info("Choose car for sale");
            carsList.GetFirstCar().Element.Click();
        }
    }
}
