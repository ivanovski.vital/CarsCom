﻿using CarsCom.Elements;
using CarsCom.Utils;
using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class FindCarValuePage : BasePage
    {
        private const String findCarValueMessage = "Find Car's Value page didn't open'";
        private const String cbxMarksMessage = "Combobox with marks didn't open'";
        private const String cbxModelsMessage = "Combobox with models didn't open'";
        private const String cbxYearsMessage = "Combobox with years didn't open'";
        private const String tbxMileageMessage = "Combobox with mileage didn't open'";
        private const String cbxStylesMessage = "Combobox with styles didn't open'";
        private const String tbxZIPMessage = "Textbox with ZIP-code didn't open'";
        private const String btnSpecificMarkMessage = "Specific mark didn't open";
        private const String btnSpecificModelMessage = "Specific model didn't open";
        private const String btnSpecificYearMessage = "Specific year didn't open";
        private const String btnSpecificStyleMessage = "Specific style didn't open";
        private const String btnEstimateMessage = "'Estimate' button didn't open'";

        private static By btnSpecificMarkLocator;
        private static By btnSpecificModelLocator;
        private static By btnSpecificYearLocator;
        private static By btnSpecificStyleLocator;

        private static readonly By findCarValueLocator = By.XPath("//cui-icon[contains(@name, 'sell')]");
        private static readonly By cbxMarksLocator = By.XPath("//select[@id = 'make']");
        private static readonly By cbxModelsLocator = By.XPath("//select[@id = 'model']");
        private static readonly By cbxYearsLocator = By.XPath("//select[@id = 'year']");
        private static readonly By cbxStylesLocator = By.XPath("//select[contains(@name, 'style')]");
        private static readonly By tbxMileageLocator = By.XPath("//input[@id = 'input-mileage']");
        private static readonly By tbxZIPLocator = By.XPath("//input[@id = 'input-zip']");
        private static readonly By btnEstimateLocator = By.XPath("//button[contains(@ng-click, 'submit')]");

        public FindCarValuePage() : base(findCarValueLocator, findCarValueMessage) { }

        private Lazy<Car> currentCar = new Lazy<Car>(() => new Car()); 
        private static EnumMarks expectedMark;
        private static EnumModels expectedModel;
        private static EnumYears expectedYear;
        private static EnumCarStyles expectedStyle;
        private String actualMark, actualModel, actualYear, actualStyle;

        private readonly Button btnEstimate = new Button(btnEstimateLocator, btnEstimateMessage);
        private readonly Combobox cbxMark = new Combobox(cbxMarksLocator, cbxMarksMessage);
        private readonly Combobox cbxModel = new Combobox(cbxModelsLocator, cbxModelsMessage);
        private readonly Combobox cbxYear = new Combobox(cbxYearsLocator, cbxYearsMessage);
        private readonly Combobox cbxStyle = new Combobox(cbxStylesLocator, cbxStylesMessage);
        private readonly Textbox tbxMileage = new Textbox(tbxMileageLocator, tbxMileageMessage);
        private readonly Textbox tbxZIP = new Textbox(tbxZIPLocator, tbxZIPMessage);

        private readonly Lazy<Button> btnSpecificMark = new Lazy<Button>(() => new Button(btnSpecificMarkLocator, btnSpecificMarkMessage));
        private readonly Lazy<Button> btnSpecificModel = new Lazy<Button>(() => new Button(btnSpecificModelLocator, btnSpecificModelMessage));
        private readonly Lazy<Button> btnSpecificYear = new Lazy<Button>(() => new Button(btnSpecificYearLocator, btnSpecificYearMessage));
        private readonly Lazy<Button> btnSpecificStyle = new Lazy<Button>(() => new Button(btnSpecificStyleLocator, btnSpecificStyleMessage));

        public void InputSpecificMark(EnumMarks mark)
        {
            Log.log.Info("Input specific mark");
            cbxMark.Click();
            btnSpecificMarkLocator = By.XPath(StringEnum.GetStringValue(mark));
            expectedMark = mark;
            btnSpecificMark.Value.Click();
        }

        public void InputSpecificModel(EnumModels model)
        {
            Log.log.Info("Input specific model");
            cbxModel.Click();
            btnSpecificModelLocator = By.XPath(StringEnum.GetStringValue(model));
            expectedModel = model;
            btnSpecificModel.Value.Click();
        }

        public void InputSpecificYear(EnumYears year)
        {
            Log.log.Info("Input specific year");
            cbxYear.Click();
            btnSpecificYearLocator = By.XPath(StringEnum.GetStringValue(year));
            expectedYear = year;
            btnSpecificYear.Value.Click();
        }

        public void InputSpecificStyle(EnumCarStyles style)
        {
            Log.log.Info("Input specific style");
            cbxStyle.Click();
            btnSpecificStyleLocator = By.XPath(StringEnum.GetStringValue(style));
            expectedStyle = style;
            btnSpecificStyle.Value.Click();
        }

        public void InputSpecificMileage(String mileage)
        {
            Log.log.Info("Input specific mileage");
            tbxMileage.SendKeys(mileage);
        }

        public void InputZIP()
        {
            Log.log.Info("Input ZIP-code");
            tbxZIP.SendKeys(Utils.Constants.ZIP);
        }

        public Car GetCar()
        {
            Log.log.Info("Get car object");
            currentCar.Value.Mark = BaseElement.GetTextFromField(btnSpecificMarkLocator);
            currentCar.Value.Model = BaseElement.GetTextFromField(btnSpecificModelLocator);
            currentCar.Value.Style = BaseElement.GetTextFromField(btnSpecificStyleLocator);
            return currentCar.Value;
        }

        public Boolean CheckInputedData()
        {
            Log.log.Info("Check inputed data");
            actualMark = BaseElement.GetTextFromField(btnSpecificMarkLocator);
            actualModel = BaseElement.GetTextFromField(btnSpecificModelLocator);
            actualYear = BaseElement.GetTextFromField(btnSpecificYearLocator);
            actualStyle = BaseElement.GetTextFromField(btnSpecificStyleLocator);

            if (StringEnum.GetStringValue(expectedMark).Contains(actualMark) &&
                StringEnum.GetStringValue(expectedModel).Contains(actualModel) &&
                StringEnum.GetStringValue(expectedYear).Contains(actualYear) &&
                StringEnum.GetStringValue(expectedStyle).Contains(actualStyle))
            {
                return true;
            }
            else { return false; }
        }

        public void EstimateCar()
        {
            Log.log.Info("Click to Estimate Car");
            btnEstimate.Click();
        }
    }
}
