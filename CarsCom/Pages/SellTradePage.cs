﻿using CarsCom.Elements;
using CarsCom.Utils;
using Framework.Pages;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    //todo: 10.10.2017 после обновления сайта на главной странице в верхнем контитуле выдапающие списки
    // заменили на кнопки с ссылкой на страницы, т.е. ранее SellTradePage был выпадающим списком, 
    // а не полноценной страницей, класс для неё предусмотрен не был, а был предусмотрен элемент SellTradeMenu 
    // по этой причине структура претерпела некоторые скорые изменения

    public class SellTradePage : BasePage
    {
        private const String sellTradePageMessage = "Sell&Trade page didn't open";
        private static readonly By sellTradePageLocator = By.XPath("//div[contains(@class, 'two-col')]//a[contains(@data-linkname, 'quick')]");

        public SellTradePage() : base(sellTradePageLocator, sellTradePageMessage){ }

        private TopMenu menuTop = new TopMenu();

        // todo: ранее два метода ниже вызывались на HomePage, т.к. всплывающий список находился на ней
        // Ниже происходит делегирование задач классу TopMenu, т.е моделирование ситуации, будто вызов происходит
        // как и ранее(до обновления 10.10.2017) с HomePage
        public void CheckOpenedItemOnTopMenu(EnumTopMenu item)
        {
            menuTop.CheckOpenedItem(item);
        }

        public void OpenPageFromOpenedItemOnTopMenu(EnumPagesOnTopMenu page)
        {
            menuTop.OpenPage(page);
        }
    }
}
