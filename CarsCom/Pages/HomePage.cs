﻿using CarsCom.Elements;
using CarsCom.Utils;
using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class HomePage : BasePage
    {
        private const String homePageMessage = "Home page didn't open";
        private const String btnAvatarMessage = "Icon with avatar message didn't open";

        private static readonly By homePageLocator = By.XPath("//body[contains(@class, 'scope')]");
        private static readonly By btnAvatarLocator = By.XPath("//span[contains(@class, 'icon')]");
        
        public HomePage() : base(homePageLocator, homePageMessage){ }

        private TopMenu menuTop = new TopMenu();
        private Lazy<Button> btnAvatar = new Lazy<Button>(() => new Button(btnAvatarLocator, btnAvatarMessage));
        private Lazy<UserSettingsPopupMenu> userSettingsMenu = new Lazy<UserSettingsPopupMenu>(
            () => new UserSettingsPopupMenu());
        private CarMenu menuCar = new CarMenu();

        public void SelectSpecificItemInMenuCar(EnumCarMenu menuCarEnum)
        {
            Log.log.Info($"Select {menuCarEnum} in menu car");
            menuCar.SelectSpecificItem(menuCarEnum);
        }

        public HomePage OpenPopupMenuWithUserSettings()
        {
            Log.log.Info("Open popup-menu with user settings");
            btnAvatar.Value.Click();
            return new HomePage();
        }

        public Boolean CheckPopupMenuWithUserSettings()
        {
            Log.log.Info("Check popup-menu with user settings");
            return userSettingsMenu.Value.CheckAppearing();
        }

        public LoginPage LoginInTheSystem()
        {
            Log.log.Info("Logging in the system");
            userSettingsMenu.Value.btnLogin.Value.DoubleClick();
            return new LoginPage();
        }

        public HomePage CheckLogging()
        {
            Log.log.Info("Check logging");
            OpenPopupMenuWithUserSettings();
            userSettingsMenu.Value.CheckLogging();
            OpenPopupMenuWithUserSettings();
            return this;
        }

        public HomePage LogoutFromTheSystem()
        {
            Log.log.Info("Logout from the system");
            userSettingsMenu.Value.btnLogout.Value.DoubleClick();
            return this;
        }

        public HomePage CheckLogouting()
        {
            Log.log.Info("Check logout from the system");
            userSettingsMenu.Value.CheckLogouting();
            return this;
        }

        public HomePage SetMarkCar(EnumMarks mark)
        {
            Log.log.Info("Set mark Car");
            menuCar.ChooseSpecificMark(mark);
            return this;
        }

        public HomePage SetModelCar(EnumModels model)
        {
            Log.log.Info("Set model Car");
            menuCar.ChooseSpecificModel(model);
            return this;
        }

        public HomePage EnterZIPCode()
        {
            Log.log.Info("Set zip-code");
            menuCar.EnterZIPCode();
            return this;
        }

        public HomePage CheckDataInCarMenu()
        {
            if (!menuCar.CheckData())
            {
                FWAssert.Fail("Not equal data in the fields");
            }
            return this;
        }

        public CarsForSalePage SearchCarsForSale()
        {
            Log.log.Info("Search cars for Sale");
            menuCar.Search();
            return new CarsForSalePage();
        }

        public OverviewPage SearchToOverview()
        {
            Log.log.Info("Search cars to Overview");
            menuCar.Search();
            return new OverviewPage();
        }

        public HomePage CheckSpecificItemInMenuCar(EnumCarMenu menuCarEnum)
        {
            if(!menuCar.CheckSpecificItem(menuCarEnum))
            {
                FWAssert.Fail("Specific item didn't open");
            }
            return this;
        }

        public HomePage SetYearCar(EnumYears year)
        {
            Log.log.Info("Set year of car");
            menuCar.ChooseSpecificYear(year);
            return this;
        }

        public Car GetCarData()
        {
            Log.log.Info("Get car data");
            return menuCar.GetCarData();
        }

        public void OpenItemOnTopMenu(EnumTopMenu topItem)
        {
            Log.log.Info("Open specific item on topmenu");
            menuTop.OpenItem(topItem);
        }
    }
}
