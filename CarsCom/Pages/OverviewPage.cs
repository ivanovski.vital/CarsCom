﻿using CarsCom.Elements;
using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class OverviewPage : BasePage
    {
        private Car currentCar;
        private String actualMark, actualModel, actualYear;
        private const String overviewPageMessage = "Overview page didn't open";
        private const String actualMarkModelYearMessage = "Actual data didn't open";
        private const String btnGetAPriceQuoteMessage = "Button 'Get a Price Quote' didn't open";

        private static readonly By overviewPageLocator = By.XPath("//div[contains(@class, 'specs')]");
        private static readonly By actualMarkModelYearLocator = By.XPath("//h1[contains(@itemprop, 'nam')]");
        private static readonly By btnGetAPriceQuoteLocator = By.XPath("//div[contains(@class, 'forSale__r')]//a[contains(@class, 'button')]");

        private readonly Button bthGetAPriceQuote = new Button(btnGetAPriceQuoteLocator, btnGetAPriceQuoteMessage);

        public OverviewPage() : base(overviewPageLocator, overviewPageMessage){ }

        public void CheckReferenceValues(Object car)
        {
            Log.log.Info("Check Reference values on overview page");
            actualYear = BaseElement.GetTextFromField(actualMarkModelYearLocator);
            actualYear = RegexParser.Parse(actualYear, StringEnum.GetStringValue(EnumRegExs.PureIntDigit))[0].ToString();

            actualMark = BaseElement.GetTextFromField(actualMarkModelYearLocator);
            actualMark = RegexParser.Parse(actualMark, StringEnum.GetStringValue(EnumRegExs.PureLetters))[0].ToString();

            actualModel = BaseElement.GetTextFromField(actualMarkModelYearLocator);
            actualModel = RegexParser.Parse(actualModel, StringEnum.GetStringValue(EnumRegExs.PureLetters))[1].ToString();

            currentCar = new Car(actualMark, actualModel, actualYear);
            if (!currentCar.Equals(car))
            {
                FWAssert.Fail("Data from specific car not equal");
            }
        }

        public PriceQuotePage ClickToGetAPriceQuote()
        {
            Log.log.Info("Click to Get a Price Quote page");
            bthGetAPriceQuote.Click();
            return new PriceQuotePage();
        }
    }
}
