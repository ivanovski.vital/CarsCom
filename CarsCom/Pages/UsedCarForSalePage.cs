﻿using CarsCom.Utils;
using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    public class UsedCarForSalePage : BasePage
    {
        private const String userCarsForSalePageMessage = "User cars for sale page didn't open";
        private const String chxOwnerSellerMessage = "Checkbox 'Owner Seller' didn't open";
        private const String cbxFixedPriceMessage = "Combobox 'Fixed Price' didn't open";
        private const String btnSpecificFixedPriceMessage = "Button with fixed price didn't open";
        private const String btnSearchListingMessage = "Button 'Search Listing' didn't open";
        private const String btnZIPMessage = "Button 'Zip-code' didn't open";
        private const String txbZIPMessage = "Textbox 'Zip-code' didn't open";
        private const String btnConfirmZIPMessage = "Button 'Confirm Zip-code' didn't open";

        private static By btnSpecificFixedPriceLocator;
        private static readonly By userCarsForSalePageLocator = By.XPath("//body[contains(@ng-app, 'used')]");
        private static readonly By chxOwnerSellerLocator = By.XPath("//p[contains(@class, 'owner')]//input[contains(@type, 'check')]");
        private static readonly By cbxFixedPriceLocator = By.XPath("//select[contains(@class, 'selec')]");
        private static readonly By btnSearchListingLocator = By.XPath("//input[contains(@type, 'ubmit')]");
        private static readonly By btnZIPLocator = By.XPath("//a[contains(@ng-click, 'zip')]");
        private static readonly By txbZIPLocator = By.XPath("//input[contains(@class, 'input')]");
        private static readonly By btnConfirmZIPLocator = By.XPath("//input[contains(@id, 'zip')]");

        private readonly Checkbox chxOwnerSeller = new Checkbox(chxOwnerSellerLocator, chxOwnerSellerMessage);
        private readonly Combobox cbxFixedPrice = new Combobox(cbxFixedPriceLocator, cbxFixedPriceMessage);
        private readonly Button btnSearchListing = new Button(btnSearchListingLocator, btnSearchListingMessage);
        private readonly Lazy<Button> btnZIP = new Lazy<Button>(() => new Button(btnZIPLocator, btnZIPMessage)); 
        private readonly Lazy<Textbox> tbxZIP = new Lazy<Textbox>(() => new Textbox(txbZIPLocator, txbZIPMessage));
        private readonly Lazy<Button> btnConfirmZIP = new Lazy<Button>(() => new Button(btnConfirmZIPLocator, btnConfirmZIPMessage)); 

        private readonly Lazy<Button> btnSpecificFixedPrice = new Lazy<Button>(
            () => new Button(btnSpecificFixedPriceLocator, btnSpecificFixedPriceMessage));

        public UsedCarForSalePage() : base(userCarsForSalePageLocator, btnSpecificFixedPriceMessage) { }

        public void CheckOwnerSeller()
        {
            Log.log.Info("Check 'Owner Seller'");
            chxOwnerSeller.Click();
        }

        public void InputPrice(EnumPrices price)
        {
            Log.log.Info("Input price of car");
            cbxFixedPrice.Click();
            btnSpecificFixedPriceLocator = By.XPath(StringEnum.GetStringValue(price));
            btnSpecificFixedPrice.Value.Click();
        }

        public CarsForSalePage SearchListing()
        {
            Log.log.Info("Open Search Listing");
            btnSearchListing.DoubleClick();
            return new CarsForSalePage();
        }

        public void CheckZIP()
        {
            Log.log.Info("Check ZIP-code'");
            btnZIP.Value.Click();
            tbxZIP.Value.SendKeys(Utils.Constants.ZIP);
            btnConfirmZIP.Value.DoubleClick();
        }
    }
}
