﻿using CarsCom.Elements;
using Framework.Elements;
using Framework.Pages;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Pages
{
    class SpecificCarPage : BasePage
    {
        private Car currentCar;
        private String actualPrice;
        private String actualMark;
        private String actualModel;
        private String actualYear;

        private const String specificCarPageMessage = "Specific car page didn't open";
        private const String actualPriceMessage = "Actual price didn't open";
        private const String actualMarkMessage = "Actual mark didn't open";
        private const String actualModelMessage = "Actual model didn't open";
        private const String actualYearMessage = "Actual model didn't open";

        private static readonly By specificCarPageLocator = By.XPath("//div[contains(@class, 'arrows')]");
        private static readonly By actiualPriceLocator = By.XPath("//div[contains(@class, 'vdp-cap-price_')]");
        private static readonly By actiualMarkModelYearLocator = By.XPath("//h1[contains(@class, 'vdp')]");

        public SpecificCarPage() : base(specificCarPageLocator, specificCarPageMessage){ }  

        public void CheckReferenceValues(Object car)
        {
            Log.log.Info("Check References values on the specific car page");
            actualPrice = BaseElement.GetTextFromField(actiualPriceLocator);
            actualPrice = RegexParser.Parse(actualPrice, StringEnum.GetStringValue(EnumRegExs.PureFloatDigit))[0].ToString();

            actualYear = BaseElement.GetTextFromField(actiualMarkModelYearLocator);
            actualYear = RegexParser.Parse(actualYear, StringEnum.GetStringValue(EnumRegExs.PureIntDigit))[0].ToString();

            actualMark = BaseElement.GetTextFromField(actiualMarkModelYearLocator);
            actualMark = RegexParser.Parse(actualMark, StringEnum.GetStringValue(EnumRegExs.PureLetters))[1].ToString();

            actualModel = BaseElement.GetTextFromField(actiualMarkModelYearLocator);
            actualModel = RegexParser.Parse(actualModel, StringEnum.GetStringValue(EnumRegExs.PureLetters))[2].ToString();

            currentCar = new Car(actualPrice, actualMark, actualModel, actualYear);
            if (!currentCar.Equals(car))
            {
                FWAssert.Fail("Data from specific car not equal");
            }
        }
    }
}
