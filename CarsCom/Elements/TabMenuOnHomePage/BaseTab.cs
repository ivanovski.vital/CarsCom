﻿using CarsCom.Utils;
using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Elements
{
    public abstract class BaseTab : BaseElement
    {
        private const String attribureDetector = "class";
        private const String contentDetector = "active";
        protected const String cbxMarkMessage = "Combobox 'Marks' didn't open";
        protected const String cbxModelMessage = "Combobox 'Model' didn't open";
        protected const String btnSpecificMarkMessage = "Specific mark didn't open";
        protected const String btnSpecificModelMessage = "Specific model didn't open";
        protected const String btnSearchMessage = "Button 'Search' didn't open";

        protected static readonly By btnSearchLocator = By.XPath("//input[@value = 'Search']");
        protected static By cmbMarkLocator;
        protected static By cmbModelLocator;

        protected static By btnSpecificMarkLocator;
        protected static By btnSpecificModelLocator;

        protected Car innerCar = new Car();
        protected String actualMark = "";
        protected String actualModel = "";
        protected Combobox cmbMark;
        protected Combobox cmbModel;
        protected static EnumMarks expectedMark = EnumMarks.Unknown;
        protected static EnumModels expectedModel = EnumModels.Unknown;

        protected readonly Lazy<Button> btnSpecificMark = new Lazy<Button>(() => new Button(btnSpecificMarkLocator, btnSpecificMarkMessage));
        protected readonly Lazy<Button> btnSpecificModel = new Lazy<Button>(() => new Button(btnSpecificModelLocator, btnSpecificModelMessage));
        protected readonly Button btnSearch = new Button(btnSearchLocator, btnSearchMessage);

        public void ChooseSpecificMark(EnumMarks mark)
        {
            Log.log.Info("Choose specific mark(base tab)");
            cmbMark.Click();
            btnSpecificMarkLocator = By.XPath(StringEnum.GetStringValue(mark));
            expectedMark = mark;
            btnSpecificMark.Value.Click();
        }

        public void ChooseSpecificModel(EnumModels model)
        {
            Log.log.Info("Choose specific model(base tab)");
            cmbModel.Click();
            btnSpecificModelLocator = By.XPath(StringEnum.GetStringValue(model));
            expectedModel = model;
            btnSpecificModel.Value.Click();
        }

        public abstract Boolean CheckData();

        public void Search()
        {
            Log.log.Info("Click search on tab");
            btnSearch.DoubleClick();
        }

        public abstract Car GetCarData();

        public Boolean CheckSpecificItem(EnumCarMenu menuCarEnum)
        {
            Log.log.Info("Check specific item on tab");
            switch (menuCarEnum)
            {
                case EnumCarMenu.SearchCarsForSale:
                    return CheckElementContent(By.XPath(StringEnum.GetStringValue(EnumCarMenu.SearchCarsForSale)), attribureDetector, contentDetector);
                case EnumCarMenu.ReadSpecsReviews:
                    return CheckElementContent(By.XPath(StringEnum.GetStringValue(EnumCarMenu.ReadSpecsReviews)), attribureDetector, contentDetector);
                default:
                    throw new Exception();
            }
        }
    }
}
