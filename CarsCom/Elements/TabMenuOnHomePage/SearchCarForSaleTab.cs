﻿using CarsCom.Utils;
using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Elements
{
    public class SearchCarForSaleTab : BaseTab
    {
        private const String btnSearchCarsForSaleMessage = "Button 'Search cars for sale' didn't open";
        private const String txbZIPMessage = "Specific model didn't open";

        private static readonly By txbZIPLocator = By.XPath("//input[@name = 'zip']");
        private static readonly By markLocator = By.XPath("//select[@name = 'make']");
        private static readonly By modelLocator = By.XPath("//select[@name = 'model']");

        public SearchCarForSaleTab()
        {
            cmbMarkLocator = markLocator;
            cmbModelLocator = modelLocator;
            cmbMark = new Combobox(cmbMarkLocator, cbxMarkMessage);
            cmbModel = new Combobox(cmbModelLocator, cbxModelMessage);
        }

        private readonly Textbox txbZIP = new Textbox(txbZIPLocator, txbZIPMessage);

        public readonly Lazy<Button> btnSearchCarsForSale = new Lazy<Button>(() =>
            new Button(By.XPath(StringEnum.GetStringValue(EnumCarMenu.SearchCarsForSale)), btnSearchCarsForSaleMessage));

        public void EnterZIPCode()
        {
            Log.log.Info("Enter ZIP-code on search tab");
            txbZIP.SendKeys(Utils.Constants.ZIP);
        }

        public override Boolean CheckData()
        {
            Log.log.Info("Check data on search tab");
            actualMark = GetTextFromField(btnSpecificMarkLocator);
            actualModel = GetTextFromField(btnSpecificModelLocator);

            if (StringEnum.GetStringValue(expectedMark).Contains(actualMark) &&
                (StringEnum.GetStringValue(expectedModel).Contains(actualModel)))
            {
                return true;
            }
            else { return false; }
        }

        public override Car GetCarData()
        {
            Log.log.Info("Get data on search tab");
            innerCar.Mark = actualMark;
            innerCar.Model = actualModel;
            return innerCar;
        }
    }
}
