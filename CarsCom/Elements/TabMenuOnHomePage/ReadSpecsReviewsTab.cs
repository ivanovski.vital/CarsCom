﻿using CarsCom.Utils;
using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Elements
{
    public class ReadSpecsReviewsTab : BaseTab
    {
        private const String btnReadSpecsReviewsMessage = "Button 'Read Specs & Reviews' didn't open";
        private const String cbxYearMessage = "Combobox 'Year' didn't open";
        private const String btnSpecificYearMessage = "Specific year didn't open";

        private static readonly By cmbYearLocator = By.XPath("//div[contains(@class, 'year')]//select[contains(@class, 'select')]");
        private static By markLocator = By.XPath("//div[contains(@class, 'make')]//select[contains(@class, 'select')]");
        private static By modelLocator = By.XPath("//div[contains(@class, 'model')]//select[contains(@class, 'select')]");

        private static By btnSpecificYearLocator;

        String actualYear = "";
        private static EnumYears expectedYear;
        private readonly Lazy<Combobox> cbxYear = new Lazy<Combobox>(() => new Combobox(cmbYearLocator, cbxYearMessage)); 

        public ReadSpecsReviewsTab()
        {
            cmbMarkLocator = markLocator;
            cmbModelLocator = modelLocator;
            cmbMark = new Combobox(cmbMarkLocator, cbxMarkMessage);
            cmbModel = new Combobox(cmbModelLocator, cbxModelMessage);
        }

        private readonly Lazy<Button> btnSpecificYear = new Lazy<Button>(() => new Button(btnSpecificYearLocator, btnSpecificYearMessage));
        public readonly Lazy<Button> btnReadSpecsReviews = new Lazy<Button>(() =>
            new Button(By.XPath(StringEnum.GetStringValue(EnumCarMenu.ReadSpecsReviews)), btnReadSpecsReviewsMessage));

        public void ChooseSpecificYear(EnumYears year)
        {
            cbxYear.Value.Click();
            btnSpecificYearLocator = By.XPath(StringEnum.GetStringValue(year));
            expectedYear = year;
            btnSpecificYear.Value.Click();
        }

        public override Boolean CheckData()
        {
            Log.log.Info("Check data on readspecs tab");
            actualMark = GetTextFromField(btnSpecificMarkLocator);
            actualModel = GetTextFromField(btnSpecificModelLocator);
            actualYear = GetTextFromField(btnSpecificYearLocator);

            if ((StringEnum.GetStringValue(expectedMark).Contains(actualMark)) &&
                (StringEnum.GetStringValue(expectedModel).Contains(actualModel)) &&
                (StringEnum.GetStringValue(expectedYear).Contains(actualYear)))
            {
                return true;
            }
            else { return false; }
        }

        public override Car GetCarData()
        {
            Log.log.Info("Get car data on readspecs tab");
            innerCar.Mark = GetTextFromField(btnSpecificMarkLocator);
            innerCar.Model = GetTextFromField(btnSpecificModelLocator);
            innerCar.Year = GetTextFromField(btnSpecificYearLocator);
            return innerCar;
        }
    }
}
