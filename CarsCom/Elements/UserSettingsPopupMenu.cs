﻿using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Elements
{
    public class UserSettingsPopupMenu : BaseElement
    {
        private const String attribureMenu = "class";
        private const String contentMenu = "active";
        private const String popupMenuMessage = "Popup menu with user settings didn't open";
        private const String btnLoginMessage = "Button 'Log in' didn't open";
        private const String btnLogoutMessage = "Button 'Log out' didn't open";

        private static readonly By popupMenuLocator = By.XPath("//div[contains(@class, 'global-nav__profile')]//div[@class = 'container']");
        private static readonly By btnLoginLocator = By.XPath("//a[contains(@data-linkname, 'log-in')]");
        private static readonly By btnLogoutLocator = By.XPath("//a[contains(@ng-click, 'logout()')]");

        public UserSettingsPopupMenu() : base(popupMenuLocator, popupMenuMessage){ }

        public Lazy<Button> btnLogin = new Lazy<Button>(() => new Button(btnLoginLocator, btnLoginMessage));
        public Lazy<Button> btnLogout = new Lazy<Button>(()=> new Button(btnLogoutLocator, btnLogoutMessage));

        public Boolean CheckAppearing()
        {
            Log.log.Info("Check popup-menu item appearing");
            return CheckElementContent(popupMenuLocator, attribureMenu, contentMenu);
        }

        public Boolean CheckLogging()
        {
            Log.log.Info("Check logging");
            return IsElementExist(btnLogoutLocator);
        }

        public Boolean CheckLogouting()
        {
            Log.log.Info("Check logouting");
            return IsElementExist(btnLoginLocator);
        }
    }
}
