﻿using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Elements
{
    public class Car
    {
        private String price = "";
        private String mark = "";
        private String model = "";
        private String year = "";
        private String style = "";
        private String mileage = "";
        private IWebElement innerElement;
        private IWebElement clickablePart;

        public Car(){ }
        public Car(String mark, String model, String year)
        {
            this.mark = mark;
            this.model = model;
            this.year = year;
        }
        public Car(String price, String mark, String model, String year) : this(mark, model, year)
        {
            this.price = price;
        }

        public String Mark { get { return mark; } set { mark = value; } }
        public String Model { get { return model; } set { model = value; } }
        public String Price { get { return price; } set { price = value; } }
        public String Year { get { return year; } set { year = value; } }
        public String Style { get { return style; } set { style = value; } }
        public String Mileage { get { return mileage; } set { mileage = value; } }
        public IWebElement Element => clickablePart;

        public void SetCar(IWebElement element)
        {
            Log.log.Info("Set car instance from list of cars");
            innerElement = element;
            clickablePart = element.FindElement(By.XPath(Utils.Constants.ClickablePartOfFirstCarAd));
            String unhandleName = RegexParser.Parse(element.Text, StringEnum.GetStringValue(EnumRegExs.UntilNewLine))[1].ToString();
            String unhandlePrice = RegexParser.Parse(element.Text, StringEnum.GetStringValue(EnumRegExs.UntilNewLine))[2].ToString();

            if(unhandleName.Contains(Utils.Constants.DetectorOfDropSaleCar))
            {
                unhandleName = RegexParser.Parse(element.Text, StringEnum.GetStringValue(EnumRegExs.UntilNewLine))[2].ToString();
                unhandlePrice = RegexParser.Parse(element.Text, StringEnum.GetStringValue(EnumRegExs.UntilNewLine))[3].ToString();
            }

            year = RegexParser.Parse(unhandleName, StringEnum.GetStringValue(EnumRegExs.PureIntDigit))[0].ToString();
            mark = RegexParser.Parse(unhandleName, StringEnum.GetStringValue(EnumRegExs.PureLetters))[0].ToString();
            model = RegexParser.Parse(unhandleName, StringEnum.GetStringValue(EnumRegExs.PureLetters))[1].ToString();
            price = RegexParser.Parse(unhandlePrice, StringEnum.GetStringValue(EnumRegExs.PureFloatDigit))[0].ToString();
        }

        public override Boolean Equals(Object car)
        {
            Log.log.Info("Entire compare car data with references values");
            var temp = car as Car;
            if (this.mark == temp.mark &&
                this.model == temp.model &&
                this.price == temp.price &&
                this.year == temp.year &&
                this.style == temp.style)
            { return true; }
            else
            { return false; }
        }

        public Boolean ContainEqualsMMS(String car)
        {
            Log.log.Info("Contain compare car data with references values");
            if(car.Contains(this.mark) &&
                car.Contains(this.model) &&
                car.Contains(this.style))
            {
                return true;
            }
            else { return false; }
        }
    }
}
