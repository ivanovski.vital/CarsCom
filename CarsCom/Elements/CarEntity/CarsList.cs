﻿using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;

namespace CarsCom.Elements
{
    public class CarsList : BaseElement
    {
        private const String tagLocation = "div";
        private const String carsListMessage = "Cars list didn't open";
        private static readonly By carsListLocator = By.XPath("//div[@id = 'srp-listing-rows-container']");

        public CarsList() : base(carsListLocator, carsListMessage) { }

        private static Car firstCar = new Car();

        private static readonly ElementsList elementListWithCars = new ElementsList(carsListLocator, tagLocation, carsListMessage);

        public Car GetFirstCar()
        {
            Log.log.Info("Filling listcar");
            var cars = new List<Car>();
            foreach (var game in elementListWithCars.ToList())
            {
                var temp = new Car();
                temp.SetCar(game);
                return temp;
            }
            return null;
        }
    }
}
