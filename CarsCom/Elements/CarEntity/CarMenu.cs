﻿using CarsCom.Utils;
using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Elements
{
    class CarMenu : BaseElement
    {
        private const String menuMessage = "Car menu didn't open";
        private static readonly By menuLocator = By.XPath("//section[contains(@class, 'page-section--h')]");

        public CarMenu() : base(menuLocator, menuMessage){ }

        private static EnumCarMenu currentTab;
        private Lazy<SearchCarForSaleTab> searchCarForSale = new Lazy<SearchCarForSaleTab>(
            () => new SearchCarForSaleTab());
        private Lazy<ReadSpecsReviewsTab> readSpecsReview = new Lazy<ReadSpecsReviewsTab>(
            () => new ReadSpecsReviewsTab());

        public void SelectSpecificItem(EnumCarMenu menuCarEnum)
        {
            Log.log.Info("Select specific item by menu car");
            switch (menuCarEnum)
            {
                case EnumCarMenu.SearchCarsForSale:
                    currentTab = EnumCarMenu.SearchCarsForSale;
                    searchCarForSale.Value.btnSearchCarsForSale.Value.Click();
                    break;
                case EnumCarMenu.ReadSpecsReviews:
                    currentTab = EnumCarMenu.ReadSpecsReviews;
                    readSpecsReview.Value.btnReadSpecsReviews.Value.Click();
                    break;
                default:
                    throw new Exception();
            }
        }

        public void ChooseSpecificMark(EnumMarks mark)
        {
            Log.log.Info("Choose specific mark from car menu");
            switch (currentTab)
            {
                case EnumCarMenu.SearchCarsForSale:
                    searchCarForSale.Value.ChooseSpecificMark(mark);
                    break;
                case EnumCarMenu.ReadSpecsReviews:
                    readSpecsReview.Value.ChooseSpecificMark(mark);
                    break;
                default:
                    throw new Exception();
            }
        }

        public void ChooseSpecificModel(EnumModels model)
        {
            Log.log.Info("Choose specific model from car menu");
            switch (currentTab)
            {
                case EnumCarMenu.SearchCarsForSale:
                    searchCarForSale.Value.ChooseSpecificModel(model);
                    break;
                case EnumCarMenu.ReadSpecsReviews:
                    readSpecsReview.Value.ChooseSpecificModel(model);
                    break;
                default:
                    throw new Exception();
            }
        }

        public void EnterZIPCode()
        {
            Log.log.Info("Enter ZIP-code for car menu");
            searchCarForSale.Value.EnterZIPCode();
        }

        public Boolean CheckData()
        {
            Log.log.Info("Check data on car menu");
            switch (currentTab)
            {
                case EnumCarMenu.SearchCarsForSale:
                    return searchCarForSale.Value.CheckData();
                case EnumCarMenu.ReadSpecsReviews:
                    return readSpecsReview.Value.CheckData();
                default:
                    throw new Exception();
            }
        }

        public void Search()
        {
            Log.log.Info("Search on car menu");
            switch (currentTab)
            {
                case EnumCarMenu.SearchCarsForSale:
                    searchCarForSale.Value.Search();
                    break;
                case EnumCarMenu.ReadSpecsReviews:
                    readSpecsReview.Value.Search();
                    break;
                default:
                    throw new Exception();
            }
        }

        public Boolean CheckSpecificItem(EnumCarMenu menuCarEnum)
        {
            Log.log.Info("Check specific item on car menu");
            switch (menuCarEnum)
            {
                case EnumCarMenu.SearchCarsForSale:
                    return searchCarForSale.Value.CheckSpecificItem(menuCarEnum);
                case EnumCarMenu.ReadSpecsReviews:
                    return readSpecsReview.Value.CheckSpecificItem(menuCarEnum);
                default:
                    throw new Exception();
            }
        }

        public void ChooseSpecificYear(EnumYears year)
        {
            Log.log.Info("Check specific year on car menu");
            readSpecsReview.Value.ChooseSpecificYear(year);
        }

        public Car GetCarData()
        {
            Log.log.Info("Get car data on car menu");
            switch (currentTab)
            {
                case EnumCarMenu.SearchCarsForSale:
                    return searchCarForSale.Value.GetCarData();
                case EnumCarMenu.ReadSpecsReviews:
                    return readSpecsReview.Value.GetCarData();
                default:
                    throw new Exception();
            }
        }
    }
}
