﻿using CarsCom.Pages;
using CarsCom.Utils;
using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;


namespace CarsCom.Elements.TopMenuOnHomePage
{
    public class SellTradeMenu : BaseElement
    {
        private const String btnOpenMessage = "Button 'Sell & Trade didn't open'";
        private const String btnHowToSellMessage = "Button 'How To Sell Your Car' didn't open";

        public SellTradeMenu(): base(By.XPath(StringEnum.GetStringValue(EnumTopMenu.SellTrade)), btnOpenMessage) { }

        private Lazy<Button> btnHowToSell = new Lazy<Button>(() => new Button(
            By.XPath(StringEnum.GetStringValue(EnumPagesOnTopMenu.HowToSellYourCar)), btnHowToSellMessage));

        public HowToSellYourCarPage OpenHowToSellYourCar()
        {
            Log.log.Info("Open 'How to Sell Car' page");
            btnHowToSell.Value.Click();
            return new HowToSellYourCarPage();
        }

    }
}
