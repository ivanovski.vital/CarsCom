﻿using CarsCom.Elements.TopMenuOnHomePage;
using CarsCom.Utils;
using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace CarsCom.Elements
{
    public class TopMenu : BaseElement
    {
        private const String menuMessage = "Top menu didn't open";
        private static readonly By menuLocator = By.XPath("//ul[contains(@class, 'global')]");

        private BuyMenu buyMenu = new BuyMenu();
        private SellTradeMenu sellTradeMenu = new SellTradeMenu();

        public TopMenu() : base(menuLocator, menuMessage){ }

        public void OpenItem(EnumTopMenu item)
        {
            Log.log.Info("Open specific item on TopMenu");
            switch (item)
            {
                case EnumTopMenu.Buy:
                    buyMenu.Click();
                    break;
                case EnumTopMenu.SellTrade:
                    sellTradeMenu.Click();
                    break;
                default:
                    throw new Exception();
            }
        }

        public Boolean CheckOpenedItem(EnumTopMenu item)
        {
            Log.log.Info("Check opened item on TopMenu");
            switch (item)
            {
                case EnumTopMenu.Buy:
                    return CheckElementContent(By.XPath(StringEnum.GetStringValue(EnumTopMenu.Buy)), "class", "active");
                case EnumTopMenu.SellTrade:
                    return CheckElementContent(By.XPath(StringEnum.GetStringValue(EnumTopMenu.SellTrade)), "class", "active");
                default:
                    throw new Exception();
            }
        }

        public void OpenPage(EnumPagesOnTopMenu page)
        {
            Log.log.Info("Check opening specific page on TopMenu");
            switch (page)
            {
                case EnumPagesOnTopMenu.HowToSellYourCar:
                    sellTradeMenu.OpenHowToSellYourCar();
                    break;
                case EnumPagesOnTopMenu.UsedCarForSale:
                    buyMenu.OpenUsedCarForSale();
                    break;
                default:
                    throw new Exception();
            }
        }
    }
}
