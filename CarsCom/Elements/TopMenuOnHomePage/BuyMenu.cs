﻿using CarsCom.Pages;
using CarsCom.Utils;
using Framework.Elements;
using Framework.Utils;
using OpenQA.Selenium;
using System;


namespace CarsCom.Elements.TopMenuOnHomePage
{
    public class BuyMenu : BaseElement
    {
        private const String btnOpenMessage = "Button 'Buy' didn't open";
        private const String btnbtnUsedCarForSaleMessage = "Button 'Used Car For Sale' didn't open";

        public BuyMenu() : base(By.XPath(StringEnum.GetStringValue(EnumTopMenu.Buy)), btnOpenMessage) { }

        private Lazy<Button> btnUsedCarForSale = new Lazy<Button>(() => new Button(
            By.XPath(StringEnum.GetStringValue(EnumPagesOnTopMenu.UsedCarForSale)), btnbtnUsedCarForSaleMessage));

        public UsedCarForSalePage OpenUsedCarForSale()
        {
            Log.log.Info("Open 'Used car for sale' page");
            btnUsedCarForSale.Value.Click();
            return new UsedCarForSalePage();
        }
    }
}
