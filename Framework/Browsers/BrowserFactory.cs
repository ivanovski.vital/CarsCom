﻿using Framework.Utils;
using OpenQA.Selenium;

namespace Framework.Browsers
{
    public abstract class BrowserFactory
    {
        protected IWebDriver driver;
        public IWebDriver GetDriver()
        {
            Log.log.Info("Get driver");
            return driver;
        }
    }
}
