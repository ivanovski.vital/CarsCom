﻿using Framework.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace Framework.Browsers
{
    public class ChromeBrowser : BaseBrowser
    {
        public ChromeBrowser()
        {
            Log.log.Info("Setting Chrome browser options");
            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("-lang=en");
            chromeOptions.AddUserProfilePreference("safebrowsing.enabled", "true");
            chromeOptions.AddUserProfilePreference("profile.default_content_settings.popups", "0");
            chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
            chromeOptions.AddUserProfilePreference("download.default_directory", Config.DirectoryForDownload);
            driver = new ChromeDriver(chromeOptions);
        }

        public override IWebDriver GetDriver()
        {
            Log.log.Info("Get Chrome browser");
            driver.Manage().Window.Maximize();
            return driver;
        }
    }
}
