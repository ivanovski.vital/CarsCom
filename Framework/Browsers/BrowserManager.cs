﻿using Framework.Utils;
using OpenQA.Selenium;

namespace Framework.Browsers
{
    public class BrowserManager : BrowserFactory
    {
        private static BrowserManager browserManager;

        private BrowserManager()
        {
            Log.log.Info("Initialize current browser");
            InitializeCurrentBrowser();
        }

        public static BrowserManager GetInstance()
        {
            Log.log.Info("Get instance of the browser");
            if (browserManager == null)
            {
                browserManager = new BrowserManager();
            }
            return browserManager;
        }

        private void InitializeCurrentBrowser()
        {
            Log.log.Info("Initializing of browser and driver");
            switch (Config.GetBrowserType())
            {
                case EnumBrowsers.Chrome:
                    driver = new ChromeBrowser().GetDriver();
                    break;
                case EnumBrowsers.Firefox:
                    driver = new FirefoxBrowser().GetDriver();
                    break;
                case EnumBrowsers.Unknown:
                    Log.log.Error("Browser not found");
                    throw new NotFoundException();
            }
            driver.Url = Config.Url;
        }

        public void Quit()
        {
            if (driver != null)
            {
                Log.log.Info("Driver has closed");
                driver.Quit();
                driver = null;
                browserManager = null;
            }
        }

        public static BrowserManager GetBrowser()
        {
            Log.log.Info("Getting browser");
            return browserManager;
        }

        public static void setDriverNull()
        {
            Log.log.Info("Set driver at null");
            browserManager = null;
        }
    }
}
