﻿using Framework.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Framework.Browsers
{
    public class FirefoxBrowser : BaseBrowser
    {
        public FirefoxBrowser()
        {
            Log.log.Info("Get Firefox profile");
            var profile = new FirefoxProfile();
            profile.SetPreference("intl.accept_languages", "en");
            profile.SetPreference("browser.download.folderList", 2);
            profile.SetPreference("browser.download.manager.alertOnEXEOpen", false);
            profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream, application/x-debian-package");
            profile.SetPreference("browser.download.manager.focusWhenStarting", false);
            profile.SetPreference("browser.download.useDownloadDir", true);
            profile.SetPreference("browser.helperApps.alwaysAsk.force", false);
            profile.SetPreference("browser.download.manager.closeWhenDone", true);
            profile.SetPreference("browser.download.manager.showAlertOnComplete", true);
            profile.SetPreference("browser.download.manager.useWindow", false);
            profile.SetPreference("browser.download.panel.shown", false);
            profile.SetPreference("geo.enabled",false);
            driver = new FirefoxDriver(profile);
        }

        public override IWebDriver GetDriver()
        {
            Log.log.Info("Get Firefox browser");
            return driver;
        }
    }
}
