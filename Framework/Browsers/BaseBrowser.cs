﻿using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace Framework.Browsers
{
    public abstract class BaseBrowser
    {
        protected IWebDriver driver;
        public abstract IWebDriver GetDriver();
        private const String comeBackScript = "window.history.go(-1)";

        public void Close()
        {
            Log.log.Info("Browser close");
            driver.Close();
        }

        public static void RefreshPage()
        {
            Log.log.Info("Refreshing of the page");
            var driver = BrowserManager.GetBrowser().GetDriver();
            driver.Navigate().Refresh();
            try
            {
                var alert = driver.SwitchTo().Alert();
                alert.Accept();
            }
            catch (NoAlertPresentException)
            {
                Log.log.Info("There was no modal window");
            }
        }

        public static void ReturnToPreviousPage()
        {
            Log.log.Info("Returning to previous page");
            var driver = BrowserManager.GetBrowser().GetDriver();
            var jScriptForComeBack = (IJavaScriptExecutor)driver;
            jScriptForComeBack.ExecuteScript(comeBackScript);
        }
    }
}
