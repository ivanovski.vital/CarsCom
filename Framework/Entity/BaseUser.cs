﻿using Framework.Utils;
using System;

namespace Framework.Entity
{
    public static class BaseUser
    {
        private static String login = Constants.login;
        private static String password = Constants.password;
        private static String oldName;

        public static String Login => login;
        public static String Password => password;

        public static String OldName { get { return oldName; } set { oldName = value; } }
    }
}
