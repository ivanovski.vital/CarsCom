﻿using Framework.Browsers;
using Framework.Utils;
using OpenQA.Selenium;
using System;

namespace Framework.Elements
{
    public abstract class BaseElement
    {
        protected IWebDriver driver;
        protected IWebElement element;
        private const String movingScript = "arguments[0].scrollIntoView(true);";

        public BaseElement() { }

        protected BaseElement(By locator, String message)
        {
            Log.log.Info($"Initialize a new element - {locator}");
            driver = BrowserManager.GetInstance().GetDriver();
            Waiter.WaitByElementIsVisible(locator);
            FWAssert.IsNotNull(driver.FindElement(locator), message);
            element = driver.FindElement(locator);
        }

        public void Click()
        {
            Log.log.Info($"Click on element - {element.Text}");
            try
            {
                element.Click();
            }
            catch(Exception)
            {
                Waiter.WaitUntilElementStalenessOf(element);
            }
        }

        public void DoubleClick()
        {
            Log.log.Info("Double click to element");
            element.Click();
            try
            {
                element.Click();
            }
            catch(StaleElementReferenceException)
            {
                Log.log.Info("One click was enough");
            }
        }

        public static Boolean IsElementExist(By locatorOfElement)
        {
            Log.log.Info($"Trying to find element - {locatorOfElement}");
            var driver = BrowserManager.GetInstance().GetDriver();
            try
            {
                driver.FindElement(locatorOfElement);
                return true;
            }
            catch (NoSuchElementException)
            {
                Waiter.WaitByElementIsVisible(locatorOfElement);
                return false;
            }
        }

        public static void MoveToElement(BaseElement element)
        {
            Log.log.Info($"Move to element - {element.element.Text}");
            var driver = BrowserManager.GetInstance().GetDriver();
            var movedElement = element.GetElement();
            ((IJavaScriptExecutor)driver).ExecuteScript(movingScript, movedElement);
        }

        public static String GetTextFromField(By locatorOffield)
        {
            Log.log.Info($"Get text from field - {locatorOffield}");
            var driver = BrowserManager.GetInstance().GetDriver();
            return driver.FindElement(locatorOffield).Text;
        }

        public IWebElement GetElement()
        {
            Log.log.Info($"Get current element - {element.Text}");
            return element;
        }

        public static String GetAttributeValue(By locator, String attributeValue)
        {
            Log.log.Info($"Getting attribute value - {locator} by {attributeValue}");
            var driver = BrowserManager.GetInstance().GetDriver();
            return driver.FindElement(locator).GetAttribute(attributeValue);
        }

        public Boolean CheckElementContent(By locatorOfElement, String attribure, String content)
        {
            Log.log.Info($"Checking content of the element - {locatorOfElement}");
            driver = BrowserManager.GetInstance().GetDriver();

            try
            {
                if (driver.FindElement(locatorOfElement).GetAttribute(attribure).Contains(content))
                {
                    return true;
                }
                return false;
            }
            catch (NullReferenceException)
            {
                FWAssert.Fail("Element don't contain any content");
                return false;
            }
        }
    }
}
