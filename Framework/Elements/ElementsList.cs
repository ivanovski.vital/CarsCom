﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using Framework.Browsers;
using Framework.Utils;

namespace Framework.Elements
{
    public class ElementsList : BaseElement
    {
        private readonly By listLocator = By.XPath("");
        private readonly String tagOfList;
        private List<IWebElement> innerElementsList = new List<IWebElement>();

        public ElementsList(By locator, String tag, String message) : base(locator, message)
        {
            tagOfList = tag;
            listLocator = locator;
        }

        public IList<IWebElement> ToList()
        {
            Log.log.Info("Getting list of elements");
            driver = BrowserManager.GetInstance().GetDriver();
            return driver.FindElement(listLocator).FindElements(By.TagName(tagOfList)).ToList();
        }
    }
}
