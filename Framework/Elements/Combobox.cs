﻿using System;
using OpenQA.Selenium;
using Framework.Utils;

namespace Framework.Elements
{
    public class Combobox : BaseElement
    {
        public Combobox(By locator, String message) : base(locator, message) { }

        public void ChooseSpecificItem(By locatorOfCombobox, By locatorOfItem)
        {
            Log.log.Info("Choosing specific item from combobox");
            driver.FindElement(locatorOfCombobox).Click();
            try
            {
                driver.FindElement(locatorOfItem).Click();
            }
            catch(NoSuchElementException)
            {
                FWAssert.Fail("Item of combobox not found");
            }
        }
    }
}
