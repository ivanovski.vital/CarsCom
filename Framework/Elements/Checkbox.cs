﻿using System;
using OpenQA.Selenium;

namespace Framework.Elements
{
    public class Checkbox : BaseElement
    {
        public Checkbox(By locator, String message) : base(locator, message) { }
    }
}
