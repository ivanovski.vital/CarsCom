﻿using System;
using OpenQA.Selenium;
using Framework.Utils;

namespace Framework.Elements
{
    public class Textbox : BaseElement
    {
        public Textbox(By locator, String message) : base(locator, message) { }

        public void SendKeys(String text)
        {
            Log.log.Info($"Send {text} to textbox");
            element.SendKeys(text);
        }

        public void Clear()
        {
            Log.log.Info($"Clear {element.Text}");
            element.Clear();
        }
    }
}
