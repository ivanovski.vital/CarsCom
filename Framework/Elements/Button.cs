﻿using OpenQA.Selenium;
using System;

namespace Framework.Elements
{
    public class Button : BaseElement
    {
        public Button(By locator, String message) : base(locator, message) { }
    }
}
