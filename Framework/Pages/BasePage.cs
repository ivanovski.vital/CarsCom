﻿using OpenQA.Selenium;
using System;
using Framework.Browsers;
using Framework.Utils;
using Framework.Elements;

namespace Framework.Pages
{
    public abstract class BasePage
    {
        protected IWebDriver driver;
        protected BasePage() { }

        protected BasePage(By locatorOfPage, String message)
        {
            Log.log.Info($"Initializing page - {locatorOfPage}");
            driver = BrowserManager.GetInstance().GetDriver();
            Waiter.WaitByExisting(locatorOfPage);
            FWAssert.IsTrue(BaseElement.IsElementExist(locatorOfPage), message);
        }

        public static Boolean IsOpenPage(By locatorOfElement)
        {
            Log.log.Info($"Checking for page open - {locatorOfElement}");
            var driver = BrowserManager.GetInstance().GetDriver();
            return driver.FindElement(locatorOfElement).Displayed;
        }

        public static void ToHomePage()
        {
            Log.log.Info("Back to home page");
            var driver = BrowserManager.GetInstance().GetDriver();
            driver.Navigate().GoToUrl(Config.Url);
        }
    }
}
