﻿using Framework.Browsers;
using OpenQA.Selenium;
using System;
using System.Text.RegularExpressions;

namespace Framework.Utils
{
    public class RegexParser
    {
        public static MatchCollection Parse(String stringForParse, String expression)
        {
            Log.log.Info($"Parsing of {stringForParse} by {expression}");
            var regex = new Regex(expression);
            return regex.Matches(stringForParse);
        }
    }
}

