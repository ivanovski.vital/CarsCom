﻿using System;
using System.Configuration;

namespace Framework.Utils
{
    public static class Config
    {
        private static readonly String nameOfBrowser = ConfigurationManager.AppSettings["nameOfBrowser"];
        private static readonly String urlForDestination = ConfigurationManager.AppSettings["urlForDestination"];
        private static readonly String mustedLanguage = ConfigurationManager.AppSettings["mustedLanguageBrowser"];
        private static readonly String directoryForDownload = ConfigurationManager.AppSettings["directoryForDownload"];

        public static String Url => urlForDestination;
        public static String MustedLanguage => mustedLanguage;
        public static String DirectoryForDownload => directoryForDownload;

        public static EnumBrowsers GetBrowserType()
        {
            Log.log.Info("Getting name of browser");
            switch (nameOfBrowser)
            {
                case "Chrome":
                    return EnumBrowsers.Chrome;
                case "Firefox":
                    return EnumBrowsers.Firefox;
                default:
                    return EnumBrowsers.Unknown;
            }
        }
    }
}
