﻿namespace Framework.Utils
{
    public enum EnumBrowsers
    {
        Unknown = 0,
        Chrome,
        Firefox
    }
}
