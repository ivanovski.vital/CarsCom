﻿namespace Framework.Utils
{
    public enum Timeouts
    {
        commonTimeout = 10000,
        minimalTimeout = 2000,
        largeTimeout = 20000
    }
}

