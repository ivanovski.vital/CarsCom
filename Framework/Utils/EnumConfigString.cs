﻿using System;

namespace Framework.Utils
{
    public static class StringEnum
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;
            var type = value.GetType();
            var fieldInfo = type.GetField(value.ToString());
            StringValue[] attrs =
                fieldInfo.GetCustomAttributes(typeof(StringValue),
                    false) as StringValue[];
            if (attrs.Length > 0)
            {
                output = attrs[0].Value;
            }
            return output;
        }

        public class StringValue : Attribute
        {
            private readonly string _value;

            public StringValue(string value)
            {
                _value = value;
            }

            public string Value
            {
                get { return _value; }
            }
        }
    }
}
