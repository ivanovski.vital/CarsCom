﻿using Framework.Browsers;
using OpenQA.Selenium.Support.UI;
using System;
using System.IO;

namespace Framework.Utils
{
    public static class FileManager
    {
        private const String fileDownloadMessage = "Failed to download file";

        public static void WaitUntilFileDownloading(String typeFile)
        {
            Log.log.Info("Waiting until file download");
            var driver = BrowserManager.GetInstance().GetDriver();
            var waiter = new WebDriverWait(driver, TimeSpan.FromSeconds((int)Timeouts.largeTimeout));
            waiter.Until(download => FileManager.IsDownloaded(Config.DirectoryForDownload, typeFile));
        }

        public static void DeleteFile(String file)
        {
            Log.log.Info("Deleting file");
            var directory = new DirectoryInfo(Config.DirectoryForDownload);
            foreach (var filef in directory.GetFiles())
            {
                if (String.Compare(filef.Name, file, StringComparison.Ordinal) == 0)
                {
                    filef.Delete();
                }
            }
        }

        public static void VerifyDownloadWithFileExtension(String file)
        {
            Log.log.Info("File loaded");
            FWAssert.IsTrue(IsDownloaded(Config.DirectoryForDownload, file), fileDownloadMessage);
        }

        public static bool IsDownloaded(String path, String file)
        {
            Log.log.Info("Check for file downloading");
            var flag = false;
            var directory = new DirectoryInfo(path);
            FileInfo[] fileInfo = directory.GetFiles();
            foreach (var filef in fileInfo)
            {
                if (String.Compare(filef.Name, file, StringComparison.Ordinal) == 0 && (filef.Exists))
                {
                    flag = true;
                }
            }
            return flag;
        }
    }
}