﻿namespace Framework.Utils
{
    public enum EnumRegExs
    {
        [StringEnum.StringValue(@"([0-9])+")] PureIntDigit,
        [StringEnum.StringValue(@"([0-9]+.[0-9]+)")] PureFloatDigit,
        [StringEnum.StringValue(@".+")] UntilNewLine,
        [StringEnum.StringValue(@"\r\n?|\n")] RemoveNR,
        [StringEnum.StringValue(@"([a-zA-Z]+)")] PureLetters
    }
}
