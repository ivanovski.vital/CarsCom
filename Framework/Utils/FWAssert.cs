﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Framework.Utils
{
    public static class FWAssert
    {
        public static void AreEqual(Object expected, Object actual, String message)
        {
            try
            {
                Assert.AreEqual(expected, actual, message);
            }
            catch (Exception)
            {
                Log.log.Error(message);
                Assert.Fail();
            }
        }

        public static void IsTrue(Boolean condition, String message)
        {
            try
            {
                Assert.IsTrue(condition, message);
            }
            catch
            {
                Log.log.Error(message);
                Assert.Fail();
            }
        }

        public static void IsNotNull(Object value, String message)
        {
            try
            {
                Assert.IsNotNull(value, message);
            }
            catch
            {
                Log.log.Error(message);
                Assert.Fail();
            }
        }

        public static void Fail(String message)
        {
            Log.log.Error(message);
            Assert.Fail();
        }
    }
}
