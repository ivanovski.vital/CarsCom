﻿using Framework.Browsers;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace Framework.Utils
{
    public static class Waiter
    {
        public static void WainUntilPageReloadWithNewAttribute(By locatorForClick, By elementWithAttribute, String attribute, String content)
        {
            Log.log.Info($"Wait {(int)Timeouts.largeTimeout} sec for loading for waiter until page reload with new attributes");
            var driver = BrowserManager.GetInstance().GetDriver();
            driver.FindElement(locatorForClick).Click();
            var waitForReload = new WebDriverWait(driver, TimeSpan.FromSeconds((Int32)Timeouts.largeTimeout));
            Func<IWebDriver, Boolean> waitForExpectedAttribute = new Func<IWebDriver, Boolean>((IWebDriver Web) =>
            {
                var elementForCheck = Web.FindElement(elementWithAttribute);
                if (elementForCheck.GetAttribute(attribute).Contains(content))
                {
                    return true;
                }
                return false;
            });
            waitForReload.Until(waitForExpectedAttribute);
        }

        public static void WaitByExisting(By locatorOfPage)
        {
            Log.log.Info($"Wait {(int)Timeouts.largeTimeout} sec for loading for existing");
            var driver = BrowserManager.GetInstance().GetDriver();
            var freeze = new WebDriverWait(driver, TimeSpan.FromSeconds((int)Timeouts.largeTimeout));
            freeze.Until(ExpectedConditions.ElementExists(locatorOfPage));
        }

        public static void WaitByElementIsVisible(By locatorOfPage)
        {
            Log.log.Info($"Wait {(int)Timeouts.largeTimeout} sec for loading for visibility of element");
            var driver = BrowserManager.GetInstance().GetDriver();
            var freeze = new WebDriverWait(driver, TimeSpan.FromSeconds((int)Timeouts.largeTimeout));
            freeze.Until(ExpectedConditions.ElementIsVisible(locatorOfPage));
        }

        public static void WaitUntilElementStalenessOf(IWebElement element)
        {
            Log.log.Info($"Wait {(int)Timeouts.commonTimeout} sec for loading for staleness of element");
            var driver = BrowserManager.GetInstance().GetDriver();
            var freeze = new WebDriverWait(driver, TimeSpan.FromSeconds((int)Timeouts.largeTimeout));
            freeze.Until(ExpectedConditions.StalenessOf(element));
        }
    }
}
