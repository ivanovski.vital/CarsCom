﻿using System;

namespace Framework.Utils
{
    public static class RandomNameGenerator
    {
        private const Int32 length = 7;
        private const String allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
        private static Char[] chars = new Char[length];
        private static Random rand = new Random();

        public static String CreateRandomName()
        {
            Log.log.Info($"Creating another random name");
            for (Int32 i = 0; i < length; i++)
            {
                chars[i] = allowedChars[rand.Next(0, allowedChars.Length)];
            }
            return new String(chars);
        }
    }
}

